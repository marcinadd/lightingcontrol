package pl.marcinadd.lightingcontrol;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class LightContent {

    static List<LightItem> ITEMS = new ArrayList<>();

    static {
        addItem(createLightItem("sonoff"));
        addItem(createLightItem("Secondary lighting"));

    }

    private static void addItem(LightItem lightItem) {
        ITEMS.add(lightItem);
    }

    private static LightItem createLightItem(String name) {
        return new LightItem(name);
    }


    public static class LightItem {
        String name;
        boolean status;
        Date lastUpdateDate;
        boolean connection;
        int position;
        int id;

        LightItem(String name) {
            this.name = name;
        }

        @NonNull
        @Override
        public String toString() {
            return name;
        }

    }


}
