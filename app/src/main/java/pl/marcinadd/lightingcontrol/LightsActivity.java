package pl.marcinadd.lightingcontrol;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;


public class LightsActivity extends FragmentActivity implements LightFragment.OnListFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lights);
    }


    @Override
    public void onListFragmentInteraction(LightContent.LightItem item) {

    }
}
