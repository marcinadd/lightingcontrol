package pl.marcinadd.lightingcontrol;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class UpdateDataService extends JobService {

    private NotificationHelper notificationHelper;

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.e("Lighting App", "Update Data started");
        notificationHelper = new NotificationHelper(getApplicationContext());
        doBackgroundWork(params);

        return true;
    }

    private void doBackgroundWork(final JobParameters parameters) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Readings readings = new Readings();
                try {
                    //Try to get info three times if fail
                    int i=0;
                    while (readings.temp == 0 && i<3) {

                        //TODO Remove this Hardcored URL
                        URL url = new URL("http://192.168.1.41/get.json");
                        BufferedReader in = new BufferedReader(
                                new InputStreamReader(url.openStream())
                        );

                        String inputLine;
                        StringBuilder stringBuilder = new StringBuilder();
                        while ((inputLine = in.readLine()) != null)
                            stringBuilder.append(inputLine);
                        in.close();
                        readings = getTempAndHum(stringBuilder.toString());
                        i++;
                    }

                    if (readings.temp>0)
                        notificationHelper.setNotification("Lighting app", notificationHelper.buildNotificationString(readings.temp, readings.hum));

                        jobFinished(parameters, false);

                        //TODO Add UpdateLocalValues and Notification Here

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }).start();
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

    private Readings getTempAndHum(String s) {
        Readings readings = new Readings();
        try {

            JSONObject jsonObject = new JSONObject(s);
            readings.temp = jsonObject.getDouble("temp");
            readings.hum = jsonObject.getDouble("hum");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return readings;


    }

    class Readings {
        double temp;
        double hum;
    }


}