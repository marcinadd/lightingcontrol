package pl.marcinadd.lightingcontrol;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class CustomMarkerView extends MarkerView {

    private final TextView tvContent;

    public CustomMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);
        tvContent = findViewById(R.id.tvContent);

    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {


        final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM HH:mm YYYY", Locale.ENGLISH);
        String date = sdf.format(ChartActivity.list.get(Math.round(e.getX())).timestamp);
        String formated = Utils.formatNumber(e.getY(), 2, true) + " " +
                date;
        tvContent.setText(formated);

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
