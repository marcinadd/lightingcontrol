package pl.marcinadd.lightingcontrol.data;

import android.provider.BaseColumns;

public final class DbContract {
    private DbContract() {
    }

    public static class DataEntry implements BaseColumns {
        public static final String TABLE_NAME = "readings";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_DEVICE_NAME = "device_name";
        public static final String COLUMN_NAME_VAL1 = "val1";
        public static final String COLUMN_NAME_VAL2 = "val2";
        public static final String COLUMN_NAME_VAL3 = "val3";

    }

}
