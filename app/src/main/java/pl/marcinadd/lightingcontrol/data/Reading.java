package pl.marcinadd.lightingcontrol.data;

import java.util.Date;

public class Reading {
    public Date timestamp;
    public float val1;
    public float val2;
    int id;
    String deviceName;
    //**not used
    //float val3;

    Reading(int id, Date timestamp, String deviceName, float val1, float val2) {
        this.id = id;
        this.timestamp = timestamp;
        this.deviceName = deviceName;
        this.val1 = val1;
        this.val2 = val2;
    }
}

