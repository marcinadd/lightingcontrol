package pl.marcinadd.lightingcontrol.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class DataToListHelper {


    private static List<Reading> get(SQLiteDatabase db, String deviceName, int rows, int limit) {

        String[] projection = {
                "id",
                DbContract.DataEntry.COLUMN_NAME_TIMESTAMP,
                DbContract.DataEntry.COLUMN_NAME_DEVICE_NAME,
                DbContract.DataEntry.COLUMN_NAME_VAL1,
                DbContract.DataEntry.COLUMN_NAME_VAL2
        };


        String selection = DbContract.DataEntry.COLUMN_NAME_DEVICE_NAME + " = ?";
        String[] selectionArgs = {deviceName};

        String sortOrder = "id" + " ASC";

        int offset = rows - limit;


        String limitParsed = String.valueOf(offset) +
                "," +
                String.valueOf(limit);


        Cursor cursor = db.query(
                DbContract.DataEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder,
                limitParsed
        );

        List<Reading> list = new ArrayList<>();

        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndexOrThrow("id"));

            String timestamp = cursor.getString(
                    cursor.getColumnIndexOrThrow(DbContract.DataEntry.COLUMN_NAME_TIMESTAMP));

            float val1 = (float) cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DbContract.DataEntry.COLUMN_NAME_VAL1));
            float val2 = (float) cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DbContract.DataEntry.COLUMN_NAME_VAL2));

            Date parsed = null;

            try {
                parsed = parseTimestamp(timestamp);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Reading reading = new Reading(id, parsed, deviceName, val1, val2);
            list.add(reading);

        }
        cursor.close();
        return list;

    }

    private static Date parseTimestamp(String timestamp) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(timestamp);
    }

    public List<Reading> getDataToList(Context context, String deviceName) {


        DbHelper mDbHelper = new DbHelper(context);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        int rows = mDbHelper.countEntries(db, deviceName);

        //****Concat  URI

        String URI = "http://" + Configuration.host_ip + ":" + Configuration.host_port + "/testv3.php?count=";

        URI += String.valueOf(rows);

        DbUpdateTask dbUpdateTask = new DbUpdateTask(context);
        dbUpdateTask.execute(URI);


        return get(db, deviceName, rows, Configuration.limit);

    }


}
