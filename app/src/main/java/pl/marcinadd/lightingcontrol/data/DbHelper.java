package pl.marcinadd.lightingcontrol.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "iot_data.db";


    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + DbContract.DataEntry.TABLE_NAME + " (" +
                    " id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    DbContract.DataEntry.COLUMN_NAME_TIMESTAMP + " DATETIME," +
                    DbContract.DataEntry.COLUMN_NAME_DEVICE_NAME + " TEXT," +
                    DbContract.DataEntry.COLUMN_NAME_VAL1 + " FLOAT," +
                    DbContract.DataEntry.COLUMN_NAME_VAL2 + " FLOAT," +
                    DbContract.DataEntry.COLUMN_NAME_VAL3 + " FLOAT" + " );";


    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + DbContract.DataEntry.TABLE_NAME;


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public int countEntries(SQLiteDatabase db, String deviceName) {
        String query =
                "SELECT COUNT(*) FROM "
                        + DbContract.DataEntry.TABLE_NAME +
                        " WHERE " + DbContract.DataEntry.COLUMN_NAME_DEVICE_NAME + "=?";

        Cursor mCount = db.rawQuery(query, new String[]{deviceName});
        mCount.moveToFirst();
        int count = mCount.getInt(0);
        mCount.close();
        Log.e("Number of entries in DB", String.valueOf(count));

        return count;

    }


}
