package pl.marcinadd.lightingcontrol;

import android.annotation.SuppressLint;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    public static String IP = "http://192.168.1.41";
    public static final int DATA_PERIOD = 15;
    public static final String GET_URL = "/get.json";
    public static final String DEVICE_NAME = "esp_room_1";
    public static final String TOPIC = "server/in";
    private SeekBar seekBar;
    private TextView textView3;
    private TextView textView2;
    private Switch switch1;
    private TextView textViewTemp;
    private TextView textViewHum;
    private NotificationHelper notificationHelper;

    MqttHelper mqttHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //notificationHelper = new NotificationHelper(getApplicationContext());

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Button button = findViewById(R.id.button);
        seekBar = findViewById(R.id.seekBar);
        textView3 = findViewById(R.id.textView3);
        switch1 = findViewById(R.id.switch1);
        textView2 = findViewById(R.id.textView2);
        textViewTemp = findViewById(R.id.textViewTemp);
        textViewHum = findViewById(R.id.textViewHum);


        scheduleUpdateDataJob();
        startMqtt();

        //Set IP Address
        //TODO Add update this address when value changed in settings
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        IP = preferences.getString("ip_value", "http://192.168.1.11");



        @SuppressLint("StaticFieldLeak")
        class GetDataTask extends AsyncTask<String, Void, String> {


            @Override
            protected String doInBackground(String... address) {


                try {
                    URL url = new URL(address[0]);
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(url.openStream())
                    );

                    String inputLine;
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((inputLine = in.readLine()) != null)
                        stringBuilder.append(inputLine);
                    in.close();

                    return stringBuilder.toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return "ERR";
            }

            @Override
            protected void onPostExecute(String result) {
                switchResult(result);
            }


        }

        new GetDataTask().execute(IP + GET_URL);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new GetDataTask().execute(IP + GET_URL);

            }
        });


        seekBar.setMax(1023);
        seekBar.setProgress(130);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView3.setText(String.valueOf(progress));
                if (progress != 0) {
                    switch1.setChecked(true);
                } else {
                    switch1.setChecked(false);
                }

                JSONObject object = new JSONObject();
                try {
                    object.put("device_name", DEVICE_NAME);
                    object.put("pwm", String.valueOf(progress));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mqttHelper.publishMessage(TOPIC, object.toString());

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                JSONObject object = new JSONObject();
                try {
                    object.put("device_name", DEVICE_NAME);
                    object.put("status", String.valueOf(isChecked));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mqttHelper.publishMessage(TOPIC, object.toString());

            }
        });


//        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
//        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
//            @Override
//            public void run() {
//                new GetDataTask().execute(IP + GET_URL);
//            }
//        }, 0, DATA_PERIOD, TimeUnit.SECONDS);


    }


    private void startMqtt(){
        mqttHelper=new MqttHelper(getApplicationContext());
        mqttHelper.mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                //Log.e("Mqttt", "Connected to mqtt");
            }

            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) {
                //Log.v("Mqtt message", message.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                MainActivity.this.startActivity(intent);
                return true;

            case R.id.action_chart:
                Intent intent2 = new Intent(MainActivity.this, ChartActivity.class);
                MainActivity.this.startActivity(intent2);
                return true;

            case R.id.action_lights:
                Intent intent3 = new Intent(MainActivity.this, LightsActivity.class);
                MainActivity.this.startActivity(intent3);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void updateLocalValuesJson(String result) {
        try {

            JSONObject jsonObject = new JSONObject(result);

            if (jsonObject.getDouble("temp") > -55) {
                textViewTemp.setText(jsonObject.getDouble("temp") + "°C");
                textViewHum.setText(jsonObject.getString("hum") + "%");
            }


            JSONArray jsonArray = jsonObject.getJSONArray("lights");
            //Light0
            JSONObject lightobj = jsonArray.getJSONObject(0);
            Log.e("Text", lightobj.getString("status"));
            if (lightobj.getString("status").equals("1")) {
                switch1.setChecked(true);
            } else {
                switch1.setChecked(false);
            }


            int value = lightobj.getInt("value");
            seekBar.setProgress(value);
            textView3.setText(value);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void switchResult(String result) {
        //TextView tv=(TextView) findViewById(R.id.textView2);
        Log.e("RESULT", result);
        switch (result) {
            case "ERR":
                //Connection failed
                Log.e("ERROR", "ERROR While Connecting");
                textView2.setText(R.string.conn_fail);
                break;
            case "OK":
                //Connection Successful
                textView2.setText(R.string.conn_success);
                break;
            default:
                //Get method to update values
                try {
                    updateLocalValuesJson(result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                textView2.setText(R.string.conn_success);
        }

    }

    public void scheduleUpdateDataJob() {
        ComponentName componentName = new ComponentName(this, UpdateDataService.class);
        JobInfo jobInfo = new JobInfo.Builder(1234, componentName)
                .setPersisted(true)
                .setPeriodic(15 * 60 * 1000)
                .build();
        JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
       int resultCode=scheduler.schedule(jobInfo);


       if (resultCode==JobScheduler.RESULT_SUCCESS){
           Log.e("tag","Job scheduled");
       }else {
           Log.e("TAG", "Job sched fail");
       }

    }


}


