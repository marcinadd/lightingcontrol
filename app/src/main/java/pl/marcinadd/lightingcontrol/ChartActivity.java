package pl.marcinadd.lightingcontrol;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import pl.marcinadd.lightingcontrol.custom.ValueFormatter;
import pl.marcinadd.lightingcontrol.data.DataToListHelper;
import pl.marcinadd.lightingcontrol.data.Reading;

public class ChartActivity extends AppCompatActivity implements OnChartValueSelectedListener {

    public static List<Reading> list;


    private LineChart chart;
    public static int rows; //ROWS in Database
    public static String dhtDeviceName = "dht_room";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);


        //****Chart

        setTitle("DataChartActivity");
        {
            chart = findViewById(R.id.chart);
            chart.setBackgroundColor(Color.WHITE);
            chart.getDescription().setEnabled(false);
            chart.setTouchEnabled(true);
            chart.setOnChartValueSelectedListener(this);
            chart.setDrawGridBackground(false);

            CustomMarkerView mv = new CustomMarkerView(this, R.layout.custom_marker_view);
            mv.setChartView(chart);
            chart.setMarker(mv);

            chart.setDragEnabled(true);
            chart.setScaleEnabled(true);
            chart.setPinchZoom(true);

        }

        XAxis xAxis;
        {
            xAxis = chart.getXAxis();
            xAxis.enableGridDashedLine(10f, 10f, 0f);

        }


        list = new DataToListHelper().getDataToList(getApplicationContext(), dhtDeviceName);

        setData(list);

        xAxis.setValueFormatter(new ValueFormatter() {
            private final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM HH:mm", Locale.ENGLISH);

            @Override
            public String getFormattedValue(float value) {
                Date d = list.get((int) value).timestamp;
                return sdf.format(d);
            }

        });


        chart.animateX(2000);

        Legend legend = chart.getLegend();
        legend.setForm(Legend.LegendForm.LINE);


    }

    private void setData(List<Reading> list) {
        ArrayList<ArrayList<Entry>> values = new ArrayList<>();
        values.add(new ArrayList<Entry>());
        values.add(new ArrayList<Entry>());


        for (int i = 0; i < list.size(); i++) {
            values.get(0).add(new Entry(i, list.get(i).val1, getResources().getDrawable(R.drawable.ic_stat_notification)));
        }

        for (int i = 0; i < list.size(); i++) {
            values.get(1).add(new Entry(i, list.get(i).val2, getResources().getDrawable(R.drawable.ic_stat_notification)));
        }


        LineDataSet set1;
        LineDataSet set2;

        set1 = new LineDataSet(values.get(0), "Temperature");
        set1.setColor(Color.RED);
        set1.setCircleColor(Color.RED);

        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);

        set1.setDrawCircleHole(false);

        set1.setAxisDependency(YAxis.AxisDependency.LEFT);

        set2 = new LineDataSet(values.get(1), "Humidity");
        set2.setColor(Color.BLUE);
        set2.setCircleColor(Color.BLUE);

        set2.setLineWidth(1f);
        set2.setCircleRadius(3f);

        set2.setDrawCircleHole(false);

        set2.setAxisDependency(YAxis.AxisDependency.RIGHT);


        ArrayList<ILineDataSet> dataSets = new ArrayList<>();

        dataSets.add(set1);
        dataSets.add(set2);


        LineData data = new LineData(dataSets);
        chart.setData(data);


    }



    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

}
