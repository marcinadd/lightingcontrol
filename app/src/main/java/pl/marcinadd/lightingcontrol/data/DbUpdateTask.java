package pl.marcinadd.lightingcontrol.data;


import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import pl.marcinadd.lightingcontrol.ChartActivity;

public class DbUpdateTask extends AsyncTask<String, Void, String> {

    private Context context;

    public DbUpdateTask(Context context) {
        this.context = context;

    }


    @Override
    protected String doInBackground(String... strings) {

        try {
            URL url = new URL(strings[0]);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(url.openStream())
            );

            String inputLine;
            StringBuilder stringBuilder = new StringBuilder();
            while ((inputLine = in.readLine()) != null)
                stringBuilder.append(inputLine);
            in.close();

            String result = stringBuilder.toString();
            JSONObject object = new JSONObject(result);
            JSONArray array = object.getJSONArray("values");
            Log.w("Array size", String.valueOf(array.length()));

            DbHelper dbHelper = new DbHelper(context);

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            for (int i = 0; i < array.length(); i++) {
                JSONObject object1 = array.getJSONObject(i);

                String device_name = object1.getString("device_name");
                String timestamp = object1.getString("timestamp");
                String val1 = object1.getString("val1");
                String val2 = object1.getString("val2");

                ContentValues values = new ContentValues();
                values.put(DbContract.DataEntry.COLUMN_NAME_DEVICE_NAME, device_name);
                values.put(DbContract.DataEntry.COLUMN_NAME_TIMESTAMP, timestamp);
                values.put(DbContract.DataEntry.COLUMN_NAME_VAL1, val1);
                values.put(DbContract.DataEntry.COLUMN_NAME_VAL2, val2);

                db.insert(DbContract.DataEntry.TABLE_NAME, null, values);

            }


            Log.i("Insert info", "Inserted successfully" + String.valueOf(array.length()) + "rows");

            ChartActivity.rows += array.length();


            return "OK";


        } catch (Exception e) {
            e.printStackTrace();
        }


        return "ERR";
    }


    @Override
    protected void onPostExecute(String s) {

    }
}
