package pl.marcinadd.lightingcontrol;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.List;


public class MyLightRecyclerViewAdapter extends RecyclerView.Adapter<MyLightRecyclerViewAdapter.ViewHolder> {

    private final List<LightContent.LightItem> mValues;
    private MqttHelper mqttHelper;

    MyLightRecyclerViewAdapter(List<LightContent.LightItem> items) {
        mValues = items;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_light, parent, false);

        startMqtt(parent.getContext());

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.bind(position);
        holder.mSwitch.setOnCheckedChangeListener(null);
        holder.mSwitch.setChecked(mValues.get(position).status);
        holder.mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String name = mValues.get(position).name;
                String topic = "cmnd/" + name + "/power";

                String payload;
                if (isChecked)
                    payload = "On";
                else
                    payload = "Off";


                mqttHelper.publishMessage(topic, payload);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private void startMqtt(Context context) {
        mqttHelper = new MqttHelper(context);
        mqttHelper.mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {

            }

            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) {

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final Switch mSwitch;
        final View mView;
        final TextView mIdView;
        final TextView mContentView;
        public int position;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = view.findViewById(R.id.item_number);
            mContentView = view.findViewById(R.id.content);
            mSwitch = view.findViewById(R.id.switch3);
        }

        void bind(int position) {
            this.position = position;
            mIdView.setText(String.valueOf(position));
            mContentView.setText(mValues.get(position).name);
            mSwitch.setChecked(mValues.get(position).status);

        }


    }
}
